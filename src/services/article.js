import { extractArticle } from '../utils';
import { BASE_URL } from '../constants/apiPaths';
// TODO handle errors
function fetchArticle() {
  return fetch(`${BASE_URL}/api/859f2008-a40a-4b92-afd0-24bb44d10124/delivery/v1/content/fa9519d5-0363-4b8d-8e1f-627d802c08a8`)
    .then(response => response.json())
    .then(response => extractArticle(response.elements));
}

export default fetchArticle;
