// Colors
export const colorBlack = '#222222';
export const colorLightGrey = '#b0b0b0';
export const colorWhite = '#ffffff';

// Spacing (mainly for paddings and margins)
export const spacingLarge = '3rem';
export const spacingMedium = '2rem';
export const spacingSmall = '1rem';
export const borderWidthThin = '1px';

// Font size
export const fontSmall = '1.4rem';
export const fontMedium = '1.8rem';
export const fontXLarge = '3rem';

// Font weight
export const fontWeightNormal = 400;
export const fontWeightBold = 700;

// Z-index
export const zIndexStickyElement = 6000;
