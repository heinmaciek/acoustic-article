import { renderHook } from '@testing-library/react-hooks';

import { FETCH_ARTICLE_MOCK } from '../../__mocks__/article';
import { useFetchArticle } from '../useFetchArticle';

describe('HOOK - useFetchArticle', () => {
  beforeEach(() => {
    fetch.resetMocks()
  })

  it('expect loading "false" when fetching article finish', async () => {
    fetch.mockResponseOnce((JSON.stringify(
      FETCH_ARTICLE_MOCK
    )));

    const { result, waitForNextUpdate } = renderHook(() => useFetchArticle());

    await waitForNextUpdate();

    expect(result.current.loading).toBe(false);
  });
});
