import { useState, useEffect } from "react";

import fetchArticle from '../services/article';

export function useFetchArticle() {
  const [article, setArticle] = useState({});
  const [loading, setLoading] = useState(true);


  const fetch = async () => {
    const article = await fetchArticle();

    setArticle(article);
    setLoading(false);
  }

  useEffect(() => {
    fetch();
  }, []);

  return { article, loading };
}