import extractArticle from '../extractArticle';

describe('FUNC - extractArticle', () => {
  it('returns article in correct format', () => {
    const initialArticle = {
      author: {
        value: 'author'
      },
      body: {
        values: ['<p>Text</p>']
      },
      heading: {
        value: 'header'
      },
      mainImage: {
        value: {
          leadImage: {
            renditions: {
              lead: {
                url: '/url'
              }
            },
            asset: {
              altText: 'altText'
            }
          },
          leadImageCaption: {
            value: 'imageCaption'
          }
        }
      }
    }

    const expectedArticle = {
      author: 'author',
      body: ['Text'],
      heading: 'header',
      mainImage: {
        url: '/url',
        altText: 'altText',
        leadImageCaption: 'imageCaption'
      }
    }
    expect(extractArticle(initialArticle)).toEqual(expectedArticle);
  });
});
