import formatDate from '../formatDate';

describe('FUNC - formatDate', () => {
  it('returns date in correct format', () => {

    expect(formatDate('December 17, 2018')).toEqual('Mon, Dec 17');
  });
});
