const REGEX = /(<([^>]+)>)/ig

function extractArticle(article) {
  const { author, body, heading, mainImage } = article;

  const image = extractMainImage(mainImage);

  const extactedBody = extractBody(body.values);

  return {
    author: author.value,
    body: extactedBody,
    heading: heading.value,
    mainImage: image
  }
}

function extractMainImage(mainImage) {
  const { value } = mainImage;

  return {
    url: value.leadImage.renditions.lead.url,
    leadImageCaption: value.leadImageCaption.value,
    altText: value.leadImage.asset.altText
  }
}

function extractBody(body) {
  return body.map((element) => extractTextFromTag(element));
}

function extractTextFromTag(tagWithText) {
  return tagWithText.replace(REGEX, '');
}

export default extractArticle;
