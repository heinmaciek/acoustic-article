function formatDate(date) {
  const newDate = new Date(date);

  return newDate.toLocaleDateString('en-US', {
    weekday: 'short',
    month: 'short',
    day: 'numeric'
  });
}

export default formatDate;
