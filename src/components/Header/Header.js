// Default website header

import React from 'react';

import Text from '../Text';
import Layout from './Layout';
import Content from './Content';

function Header() {
  return (
    <Layout>
      <Content as="nav">
        <Text black>Simple Article</Text>
      </Content>
    </Layout>
  );
}

export default Header;
