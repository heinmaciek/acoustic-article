import styled from 'styled-components';

import ContentBase from '../Content';

import { spacingMedium } from '../../styles/designTokens';

const Content = styled(ContentBase)`
  padding: 0 ${spacingMedium};
`;

export default Content;
