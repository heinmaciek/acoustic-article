import '../../styles/sanitize.css';

import React from 'react';

import App from '../App';
import GlobalStyle from '../GlobalStyle';

function Root() {
  return (
    <>
      <GlobalStyle />
      <App />
    </>
  );
}

export default Root;
