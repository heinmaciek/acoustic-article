import styled from 'styled-components';

import { colorLightGrey, spacingSmall, fontMedium, fontWeightNormal, colorBlack } from '../../styles/designTokens';

const Text = styled.span`
  color: ${({ black }) => (black ? colorBlack : colorLightGrey)};
  padding: 0 ${spacingSmall};
  font-weight: ${fontWeightNormal};
  font-size: ${fontMedium};
  text-transform: none;
  background: none;
  border: 0;
`;

export default Text;