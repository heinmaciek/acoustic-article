import React from 'react';
import { create } from 'react-test-renderer';

import Text from '../Text';

describe('COMPONENT - Text', () => {
  it('renders correctly', () => {
    const component = create(<Text />);

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('renders correctly "black" color', () => {
    const component = create(<Text black />);

    expect(component.toJSON()).toMatchSnapshot();
  });
});
