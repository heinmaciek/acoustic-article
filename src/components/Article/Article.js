import React from 'react';

import { useFetchArticle } from '../../hooks/useFetchArticle';
import { formatDate } from '../../utils';
import { BASE_URL } from '../../constants/apiPaths';
import Content from '../Content';
import Headline from '../Headline';
import Text from '../Text';
import MainImage from '../MainImage';
import Header from './Header';
import Body from '../Body';

const DATE = 'December 17, 2018';

function Article() {
  const formatedDate = formatDate(DATE);

  const { article, loading } = useFetchArticle();

  const { author, body, heading, mainImage } = article;

  const renderArticle = () => loading ?
    <div>Loading...</div> :
    <>
      <Header data-testid="header">
        <Text>{author}</Text>
        <Text>{formatedDate}</Text>
      </Header>
      <Headline>{heading}</Headline>
      <MainImage src={`${BASE_URL}${mainImage.url}`} alt={mainImage.altText} description={mainImage.leadImageCaption} />
      <Body body={body} />
    </>

  return <Content>
    {renderArticle()}
  </Content>
}

export default Article;