import React from 'react';
import { render, waitForElement, cleanup } from '@testing-library/react';

import { FETCH_ARTICLE_MOCK } from '../../../__mocks__/article';
import Article from '../Article';

afterEach(cleanup);

describe('COMPONENT - Article', () => {
  beforeEach(() => {
    fetch.resetMocks()
  })

  it('renders correctly', async () => {
    fetch.mockResponseOnce((JSON.stringify(
      FETCH_ARTICLE_MOCK
    )));

    const { getByTestId } = render(<Article />);

    const header = await waitForElement(() => getByTestId('header'));

    expect(header).toBeDefined();
  });
});

