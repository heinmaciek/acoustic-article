import React from 'react';

import Header from '../Header';
import Article from '../Article';
import Main from './Main'

function App() {
  return (
    <>
      <Header />
      <Main>
        <Article />
      </Main>
    </>
  );
}

export default App;