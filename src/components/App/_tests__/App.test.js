import React from 'react';
import { create } from 'react-test-renderer';

import App from '../App';

jest.mock('../../Article', () => 'Article');

describe('COMPONENT - App', () => {
  it('renders correctly', () => {
    const component = create(<App />);

    expect(component.toJSON()).toMatchSnapshot();
  });
});
