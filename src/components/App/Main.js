// Whole page wrapper with accomodation for fixed header

import styled from 'styled-components';

const Main = styled.main`
  margin-top: 6rem;
`;

export default Main;
