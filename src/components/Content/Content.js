// Generic responsive content wrapper

import styled from 'styled-components';

import { spacingMedium, spacingLarge } from '../../styles/designTokens';

const Content = styled.section`
  max-width: 100rem;
  padding: ${spacingLarge} ${spacingMedium};
  margin: 0 auto;
`;

export default Content;
