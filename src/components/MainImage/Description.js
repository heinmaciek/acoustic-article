import styled from 'styled-components';

import { fontSmall } from '../../styles/designTokens';
import Text from '../Text';

const Description = styled(Text)`
  font-size: ${fontSmall};
`;

export default Description;
