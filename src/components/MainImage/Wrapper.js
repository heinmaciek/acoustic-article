import styled from 'styled-components';

const Wrapper = styled.figure`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0;
`;

export default Wrapper;
