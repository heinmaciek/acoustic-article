import React from 'react';
import { create } from 'react-test-renderer';

import MainImage from '../MainImage';

describe('COMPONENT - MainImage', () => {
  it('renders correctly', () => {
    const component = create(<MainImage src="/path" alt="test" description="test" />);

    expect(component.toJSON()).toMatchSnapshot();
  });
});
