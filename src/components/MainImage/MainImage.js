import React from 'react';
import PropTypes from 'prop-types';

import Wrapper from './Wrapper';
import Description from './Description';
import Image from './Image';

function MainImage({ src, alt, description }) {
  return <Wrapper>
    <Image src={src} alt={alt}></Image>
    <Description as="figcaption" >{description}</Description>
  </Wrapper>
}

MainImage.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
};

export default MainImage;