import React from 'react';
import { create } from 'react-test-renderer';

import Body from '../Body';

describe('COMPONENT - Body', () => {
  it('renders correctly', () => {
    const component = create(<Body body={['Simple test']} />);

    expect(component.toJSON()).toMatchSnapshot();
  });
});
