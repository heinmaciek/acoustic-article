import React from 'react';
import PropTypes, { string } from 'prop-types';

import Text from '../Text'

function Body({ body }) {
  return <>
    {
      body.map((value, key) => <Text as="p" black key={key}>{value}</Text>)
    }
  </>
}

Body.propTypes = {
  body: PropTypes.arrayOf(string).isRequired,
};

export default Body;
