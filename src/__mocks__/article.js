export const FETCH_ARTICLE_MOCK = {
  elements: {
    author: {
      value: 'author'
    },
    body: {
      values: ['<p>Text</p>']
    },
    heading: {
      value: 'header'
    },
    mainImage: {
      value: {
        leadImage: {
          renditions: {
            lead: {
              url: '/url'
            }
          },
          asset: {
            altText: 'altText'
          }
        },
        leadImageCaption: {
          value: 'imageCaption'
        }
      }
    }
  }
}